from ap_decorators import trace


def fact(n: int) -> int:
    """
    Renvoie n!
    Condition d'utilisation : n >= 0
    """
    if n == 0:
       res = 1
    else:
       res = n * fact(n - 1)
    return res




def somme_2_nombres(x : int, y :int):
    """Effectue la somme de deux nombres de manière récursive

    Précondition : Aucune
    Exemple(s) :
    $$$ somme_2_nombres(5,4)
    9
    $$$ somme_2_nombres(5,-4)
    1
    $$$ somme_2_nombres(-5,-4)
    -9
    
    """
    if y == 0:
        return x
    elif y < 0:
        return somme_2_nombres(x-1,y+1)
    else:
        return somme_2_nombres(x+1,y-1)
    

def binomial(n : int, p :int):
    """Effectue le coefficient binomial de n et p de manière récursive

    Précondition : n > 0 et p > 0
    Exemple(s) :
    $$$ binomial(4,2)
    6
    $$$ binomial(3,2)
    3
    """
    
    if p == 0 or p == n:
        return 1
    else:
        return binomial(n-1,p-1) + binomial(n-1,p) 
        
        
    
def est_palindrome(mot : str):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : Aucune
    Exemple(s) :
    $$$ est_palindrome("elle")
    True
    $$$ est_palindrome("radar")
    True
    $$$ est_palindrome("mot")
    False

    """
    i = 0
    if len(mot) == 1 or mot[0] == mot[-1]:
        return True
    if mot[0] != mot[-1]:
        return False
    elif len(mot) % 2 != 0:
        return est_palindrome(mot[:len(mot)//2] + mot[len(mot)//2+1:])
    else:
        return est_palindrome(mot[1::-1])
        

