import turtle

turtle.clearscreen()
turtle.penup()
turtle.goto(-300,100)
turtle.speed(3)
# 
# 
# for i in range(5):
#     turtle.pendown()
#     turtle.seth(30)
#     turtle.forward(20)
#     turtle.seth(-30)
#     turtle.forward(20)
    
 
#La relation entre la longueur de tracé ℓ à l'ordre n et à l'ordre n+1 est qu'il faut un tiers du segment
def courbe_Von_Koch(n,l):
    """dessine une courbe de Von Koch

    Précondition : n >= 0
    Exemple(s) : Aucun

    """
    turtle.pendown()
    if n == 0 :
        turtle.forward(l)
    else:
        courbe_Von_Koch(n-1,l//3)
        turtle.left(60)
        courbe_Von_Koch(n-1,l//3)
        turtle.right(120)
        courbe_Von_Koch(n-1,l//3)
        turtle.left(60)
        courbe_Von_Koch(n-1,l//3)
        
        
        
    
             
#courbe_Von_Koch(5,800)

def flocon(n,l):
    
    turtle.pendown()
    courbe_Von_Koch(n,l)
    turtle.right(120)
    courbe_Von_Koch(n,l)
    turtle.right(120)
    courbe_Von_Koch(n,l)

    
#flocon(4,500)

#La relation entre la longueur de tracé ℓ à l'ordre n et à l'ordre n+1 est qu'il faut un quart du segment

 
def courbe_cesaro(n,l):
    """Dessine une courbe de Cesaro

    Précondition : n >= 0 et l >= 0
    Exemple(s) : Aucune

    """
    turtle.pendown()
    if n == 0 :
        turtle.forward(l)
    else:
        courbe_cesaro(n-1,l//4)
        turtle.left(85)
        courbe_cesaro(n-1,l//4)
        turtle.right(170)
        courbe_cesaro(n-1,l//4)
        turtle.left(85)
        courbe_cesaro(n-1,l//4)
    
#courbe_cesaro(5,5000)

#La relation entre la longueur de tracé ℓ à l'ordre n et à l'ordre n+1 est qu'il faut la moitié du segment


def triangle(n,l):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """ 
    turtle.pendown()
    if n > 0 :
        for i in range(3):
            triangle(n-1,l//2)
            turtle.forward(l)
            turtle.left(120)
            
        
        
    
    
triangle(4,500)

    