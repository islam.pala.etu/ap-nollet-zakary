from ap_decorators import count


def fibo(n):
    """ renvoie le terme de la suite de Fibonacci.

    Précondition : n > 0 
    Exemple(s) :
    $$$ fibo(10)
    55

    """
    
    if n <= 2 :
        return 1
    else:
        return fibo(n-1) + fibo(n-2)
    
    
#print(fibo(40))
#La valeur met beaucoup de temps à apparaitre
        
    


