#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Noms : Nollet
# Prenoms : Zakary
# Groupe : GRP 15
# Date : 25/01/24



"""

:mod: module `recursivite`
:author: FIL - Faculté des Sciences et Technologies - Univ. Lille
:link: <http://portail.fil.univ-lille1.fr>_
:date: Mars 2020
:dernière révision: janvier 2024

"""

from ap_decorators import count, trace
# l'instruction suivante permet d'annoter des paramètres qui sont des functions.
from collections.abc import Callable


def taille_binaire(naturel: int) -> int:
    """
    Renvoie le nombre de chiffres dans l'écriture binaire de l'entier naturel `naturel`

    Précondition :
       naturel >= 0

    Exemples :
    $$$ taille_binaire(0)
    1
    $$$ taille_binaire(1)
    1
    $$$ taille_binaire(2)
    2
    $$$ taille_binaire(1023)
    10
    $$$ taille_binaire(1024)
    11
    $$$ from random import randrange
    $$$ l = [randrange(1,2**100)  for _ in range(100)]
    $$$ all(taille_binaire(elt) == len(bin(elt))-2  for elt in l)
    True

    """
    res = 1
    while naturel >= 2:
        res += 1
        naturel //= 2
    return res

def taille_binaire_recursive(naturel: int) -> int:
    """
    Renvoie le nombre de chiffres dans l'écriture binaire de l'entier naturel `naturel`

    Précondition :
       naturel >= 0
 
    Exemples :
    $$$ taille_binaire_recursive(0)
    1
    $$$ taille_binaire_recursive(1)
    1
    $$$ taille_binaire_recursive(2)
    2
    $$$ taille_binaire_recursive(1023)
    10
    $$$ taille_binaire_recursive(1024)
    11
    $$$ from random import randrange
    $$$ l = [randrange(1,2**100)  for _ in range(100)]
    $$$ all(taille_binaire_recursive(elt) == len(bin(elt))-2  for elt in l)
    True
    """
    res = 1
    if naturel < 2:
        return res 
    else:
        return res + taille_binaire_recursive(naturel//2)


taille_binaire_recursive(2)
    
def poids_binaire(naturel: int) -> int:
    """
    Renvoie le nombre de chiffre 1 dans l'écriture binaire de l'entier naturel `naturel`

    Précondition :
       naturel >= 0

    Exxemples :

    $$$ poids_binaire(0)
    0
    $$$ poids_binaire(1)
    1
    $$$ poids_binaire(2)
    1
    $$$ poids_binaire(255)
    8
    $$$ from random import randrange
    $$$ l = [randrange(1,2**100)  for _ in range(100)]
    $$$ all([poids_binaire(x)==bin(x).count('1') for x in l])
    True
    """
    res = naturel % 2
    while naturel > 0:
        naturel //= 2
        res += naturel % 2
    return res


def poids_binaire_recursif(naturel: int) -> int:
    """
    Renvoie le nombre de chiffre 1 dans l'écriture binaire de l'entier naturel `naturel`

    Précondition :
       naturel >= 0

    Exxemples :

    $$$ poids_binaire_recursif(0)
    0
    $$$ poids_binaire_recursif(1)
    1
    $$$ poids_binaire_recursif(2)
    1
    $$$ poids_binaire_recursif(255)
    8
    $$$ from random import randrange
    $$$ l = [randrange(1, 2**100)  for _ in range(100)]
    $$$ all([poids_binaire_recursif(x)==bin(x).count('1') for x in l])
    True
    """
    res = naturel % 2
    if naturel == 0:
        return res
    else:
        return res + poids_binaire_recursif(naturel//2)

def puissance(x: int|float, n: int) -> int|float:
    """
    Calcule x élevé à la puissance n

    Précondition :
        n>=0

    Exemples :

    $$$ puissance(10, 0)
    1
    $$$ puissance(10, 1)
    10
    $$$ puissance(2, 10)
    1024
    """
    return x**n
    

@count

def fois(x: int|float, y: int|float) -> int|float:
    """
    renvoie le produit de x par y

    Précondition : les mêmes que l'opérateur *

    Exemples :
    $$$ fois(8, 7)
    56
    """
    return x * y

def puissance_v2(x: int|float, n: int) -> int|float:
    """
    calcule x élevé à la puissance n

    Précondition :   n>=0

    Exemples :
    $$$ puissance_v2(10,0)
    1
    $$$ puissance_v2(10,1)
    10
    $$$ puissance_v2(2,10)
    1024
    """
    res = x
    if n == 0:
        return 1
    elif n == 1:
        return res
    else:
        return fois(res , puissance_v2(x,n-1))


#Le paramètre est la fonction puissance
#le résultat est une liste de 0

def comptage(puissance: Callable[[int|float, int], int|float]) -> list[int]:
    """
    Renvoie une liste de longueur 100 contenant le nombre de multiplications
    effectuées par la fonction ``puissance`` passée en paramètre

    Précondition :
       la fonction doit être implantée en utilisant la fonction ``fois``
    """
    res = []
    for i in range(100):
        fois.counter = 0
        _ = puissance(2, i)
        res.append(fois.counter)
    return res

print(comptage(puissance))
#on obtient ce résultat car fois.counter = 0 et on appelle pas fois


#print(comptage(puissance_v2))
#[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99]

#Le cout est de n-1 pour puissance(x,n)
#La puissance permet de multiplier n-1 fois x*x puisque la puissance effectue une multiplication à partir de 2 


 
#@trace
def puissance_calbuth(x: int|float, n: int) -> int|float:
    """
    calcule  x élevé à la puissance n

    Précondition :
        n>=0

    Exemples :

    $$$ puissance_calbuth(10,0)
    1
    $$$ puissance_calbuth(10,1)
    10
    $$$ puissance_calbuth(2,10)
    1024

    """
    if n == 0:
        return 1
    if n == 1:
        return x
    else:
        k = n // 2
        return puissance_calbuth(x, k) * puissance_calbuth(x, n - k)

#si n = k*2 l'égalité est vérifiée
puissance_calbuth(2,5)
#puissance_calbuth(2,2) est calculé 2 fois

def puissance_calbuth_v2(x: int|float, n: int) -> int|float:
    """
    calcule  x élevé à la puissance n

    Précondition :
        n>=0

    Exemples :

    $$$ puissance_calbuth_v2(10,0)
    1
    $$$ puissance_calbuth_v2(10,1)
    10
    $$$ puissance_calbuth_v2(2,10)
    1024

    """
    if n == 0:
        return 1
    if n == 1:
        return x
    else:
        k = n // 2
        return fois(puissance_calbuth_v2(x, k),puissance_calbuth_v2(x, n - k))
    
puissance_calbuth_v2(2,10) 

#Non elle ne permet pas de diminuer le coût
#Non il n'y a pas de perte
 

#if n%2 == 1
#   return puissance_calbuth_v2_amelioree(x,n-1)

dic = {}
def puissance_calbuth_v2_amelioree(x: int|float, n: int) -> int|float:
    """
    calcule  x élevé à la puissance n

    Précondition :
        n>=0

    Exemples :

    $$$ puissance_calbuth_v2_amelioree(10,0)
    1
    $$$ puissance_calbuth_v2_amelioree(10,1)
    10
    $$$ puissance_calbuth_v2_amelioree(2,10)
    1024

    """
    k = n//2
    if n == 0:
        return 1
    elif n == 1: 
        return x
    elif (x,n) in dic:
        return dic[(x,n)]*puissance_calbuth_v2_amelioree(x,k-1)
    elif n%2 == 1:
        return fois(x,puissance_calbuth_v2_amelioree(x,n-1))
    else:
        dic[(x,n)] = fois(x,n)
        return fois(puissance_calbuth_v2_amelioree(x, k),puissance_calbuth_v2_amelioree(x, n - k))
        
        
print(puissance_calbuth_v2_amelioree(2,10)) 
        
        
def comptage2(puissance: Callable[[int|float, int], int|float]) -> list[int]:
    """
    Renvoie une liste de longueur 100 contenant le nombre de multiplications
    effectuées par la fonction ``puissance`` passée en paramètre

    Précondition :
       la fonction doit être implantée en utilisant la fonction ``fois``
    """
    res = []
    for i in range(100):
        fois.counter = 0
        _ = puissance_calbuth_v2_amelioree(2, i)
        res.append(fois.counter)
    return res

print(comptage2(puissance_calbuth_v2_amelioree))

#[0, 0, 0, 1, 0, 1, 4, 1, 2, 2, 0, 1, 2, 2, 4, 1, 4, 2, 6, 2, 2, 3, 4, 1, 4, 2, 6, 2, 2, 3, 4, 1, 4, 2, 6, 2, 4, 3, 6, 2, 6, 3, 8, 3, 2, 4, 4, 1, 4, 2, 6, 2, 4, 3, 6, 2, 6, 3, 8, 3, 2, 4, 4, 1, 4, 2, 6, 2, 4, 3, 6, 2, 6, 3, 8, 3, 4, 4, 6, 2, 6, 3, 8, 3, 6, 4, 8, 3, 8, 4, 10, 4, 2, 5, 4, 1, 4, 2, 6, 2]
#L'idée de raymond réduit la compléxité de la fonction


liste = [taille_binaire_recursive(i) + poids_binaire_recursif(i) for i in range(100)]
print(liste)
def puissance_erronee(x: int|float, n: int) -> int|float:
    """
    aurait dû calculer  x élevé à la puissance n

    Précondition :
       n >= 0

    Exemples :

    $$$ puissance_erronee(10, 0)
    1
    $$$ puissance_erronee(10, 1)
    10
    $$$ #$$$ puissance_erronee(2, 10)
    $$$ #1024
    """
    if n == 0:
        return 1
    elif n == 1:
        return x
    else:
        r = n % 2
        q = n // 2
        return puissance_erronee(x, r) * puissance_erronee(puissance_erronee(x, q), 2)


