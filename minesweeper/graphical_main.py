from minesweeper import*
from graphicalboard import*
import sys

def main():
    """Lance le jeu de démineur
    """
    largeur = int(sys.argv[1])
    hauteur = int(sys.argv[2])
    nbombes = int(sys.argv[3])
    
    return (largeur,hauteur,nbombes)


if __name__ == "__main__":
    if len(sys.argv) != 4:
        game = Minesweeper(20, 20, 1)
        create(game)
    else:
        game = Minesweeper(largeur,hauteur,nbombes)
        create(game)