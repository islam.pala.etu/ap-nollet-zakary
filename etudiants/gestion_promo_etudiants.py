from date import Date
from etudiant import Etudiant

def pour_tous(seq_bool: list[bool]) -> bool:
    """
    Renvoie True ssi `seq_bool` ne contient pas False

    Exemples:

    $$$ pour_tous([])
    True
    $$$ pour_tous((True, True, True))
    True
    $$$ pour_tous((True, False, True))
    False
    """
    
    i = 0
    for elt in seq_bool:
        if elt == False:
            i +=1
    
    return i == 0
            
        
    
    

def il_existe(seq_bool: list[bool]) -> bool:
    """

    Renvoie True si seq_bool contient au moins une valeur True, False sinon

    Exemples:

    $$$ il_existe([])
    False
    $$$ il_existe((False, True, False))
    True
    $$$ il_existe((False, False))
    False
    """
    
    i = 0
    for elt in seq_bool:
        if elt == True:
            i +=1
    
    return i >= 1


def charge_fichier_etudiants(fname: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants présents dans le fichier dont
    le nom est donné en paramètre.

    précondition: le fichier est du bon format.
    """ 
    res = []
    with open(fname, 'r') as fin:
        fin.readline()
        ligne = fin.readline()
        while ligne != '':
            nip, nom, prenom, naissance, formation, groupe = ligne.strip().split(';')
            y, m, d = naissance.split('-')
            date_naiss = Date(int(d.lstrip('0')), int(m.lstrip('0')), int(y))
            res.append(Etudiant(nip, nom, prenom, date_naiss, formation, groupe))
            ligne = fin.readline()
    return res

    
etu = Etudiant(314159, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')
etu4 = Etudiant(141442, 'Calbuth', 'Raymond', Date(2,1,2005), 'MI', '11')



L_ETUDIANTS = charge_fichier_etudiants("etudiants.csv")
print(L_ETUDIANTS)
COURTE_LISTE = [L_ETUDIANTS[i] for i in range(10)]
print(COURTE_LISTE)
print(type(Etudiant(314159, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')))
print(repr(etu))
print(etu.nom)


def est_liste_d_etudiants(x) -> bool:
    """
    Renvoie True si ``x`` est une liste de fiches d'étudiant, False dans le cas contraire.

    Précondition: aucune

    Exemples:

    $$$ est_liste_d_etudiants(COURTE_LISTE)
    True
    $$$ est_liste_d_etudiants("Timoleon")
    False
    $$$ est_liste_d_etudiants([('12345678', 'Calbuth', 'Raymond', 'Danse', '12') ])
    False
    """
    
    i = 0
    if type(x) == list:
        for elt in x:
            if type(elt) != Etudiant:
                i +=1     
    else:
        i +=1
        
    return i == 0
        
    


NBRE_ETUDIANTS = len(L_ETUDIANTS)
NIP = 42312241


print(L_ETUDIANTS[NIP%603])

    
MOINS_20_ANS = [L_ETUDIANTS[i] for i in range(NBRE_ETUDIANTS) if L_ETUDIANTS[i].naissance.__gt__(Date(2,2,2004))]
    
    
print(MOINS_20_ANS)



def ensemble_des_formations(liste: list[Etudiant]) -> set[str]:
    """
    Renvoie un ensemble de chaînes de caractères donnant les formations
    présentes dans les fiches d'étudiants

    Précondition: liste ne contient que des fiches d'étudiants

    Exemples:

    $$$ ensemble_des_formations(COURTE_LISTE)
    {'MI', 'PEIP', 'MIASHS', 'LICAM'}
    $$$ ensemble_des_formations(COURTE_LISTE[0:2])
    {'MI', 'MIASHS'}
    """
    
    res = set()
    for etud in liste:
        formation = etud.formation
        if formation not in res:
            res.add(formation)
    return res



print(ensemble_des_formations(L_ETUDIANTS))
print(ensemble_des_formations(COURTE_LISTE[0:2]))


def nb_occurrences_prenom(liste):
    """affiche le nombre d'occurrences d'un prénom dans une liste d'étudiants

    Précondition : Aucune
    Exemple(s) :
    $$$ nb_occurrences_prenom(COURTE_LISTE)
    {'Eugène': 1, 'Josette': 1, 'Benoît': 1, 'David': 2, 'Andrée': 1, 'Cécile': 1, 'Christiane': 1, 'Paul': 1, 'Anne': 1}

    """
    
    res = {}
    for etud in liste:
        prenom = etud.prenom
        if prenom in res:
            res[prenom] = res[prenom] + 1
        else:
            res[prenom] = 1
    return res

print(len(nb_occurrences_prenom(L_ETUDIANTS)))


#Il y a 198 prénoms différents parmi tout les étudiants
#C'est margot avec 9 occurrences

print(nb_occurrences_prenom(L_ETUDIANTS)) 

def occurrences_nip(liste):
    """ vérifie que les identifiants (nip) des étudiants sont tous distincts.

    Précondition : Aucune
    Exemple(s) :
    $$$ 

    """
    res = {}
    for etud in liste:
        nip = etud.nip
        if nip in res:
            res[nip] = res[nip] + 1
        else:
            res[nip] = 1
            
    for nip in res:
        if res[nip] >= 2:
            return False
    
    return True
    

print(occurrences_nip(L_ETUDIANTS))



def liste_formation(liste: list[Etudiant], form: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants de la formation ``form``

    Précondition:  liste ne contient que des fiches d'étudiants

    Exemples:

    $$$ l_MI = liste_formation(COURTE_LISTE, 'MI')
    $$$ len(l_MI)
    6
    $$$ type(l_MI[1]) == Etudiant
    True
    $$$ len(liste_formation(L_ETUDIANTS, 'INFO'))
    0
    """

def liste_formation(liste: list[Etudiant], form: str) -> list[Etudiant]:
    liste_etu = [liste[i] for i in range(len(liste)) if liste[i].formation == form]
    
    return liste_etu


print(liste_formation(COURTE_LISTE, 'MI'))