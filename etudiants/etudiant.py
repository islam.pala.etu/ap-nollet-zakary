#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:author: FIL - FST - Univ. Lille.fr <http://portail.fil.univ-lille.fr>_
:date: janvier 2019
:last revised: 
:Fournit :
"""
from date import Date


class Etudiant:
    """
    une classe représentant des étudiants.

    $$$ etu = Etudiant(314159, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')
    $$$ str(etu)
    'Tim Oléon'
    $$$ repr(etu)
    '314159 : Tim OLÉON'
    $$$ etu.prenom
    'Tim'
    $$$ etu.nip
    314159
    $$$ etu.nom
    'Oléon'
    $$$ etu.formation
    'MI'
    $$$ etu.groupe
    '15'
    $$$ etu2 = Etudiant(314159, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')
    $$$ etu == etu2
    True
    $$$ etu3 = Etudiant(141442, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')
    $$$ etu == etu3
    False
    $$$ etu4 = Etudiant(141442, 'Calbuth', 'Raymond', Date(2,1,2005), 'MI', '11')
    $$$ etu < etu4
    True
    $$$ isinstance(etu.naissance, Date)
    True
    """
    def __init__(self, nip: int, nom: str, prenom: str,
                 naissance: Date, formation: str, groupe: str):
        """
        initialise un nouvel étudiant à partir de son nip, son nom, son
        prénom, sa formation et son groupe.

        précondition : le nip, le nom et le prénom ne peuvent être nuls ou vides.
        """
        
        assert nip != None
        assert prenom != None
        assert nom != None
        self.nip = nip
        self.nom = nom
        self.prenom = prenom
        self.naissance = naissance
        self.formation = formation
        self.groupe = groupe
        
        

    def __eq__(self, other) -> bool:
        """
        Renvoie True ssi other est un étudiant ayant :
        - même nip,
        - même nom et
        - même prénom que `self`,
        et False sinon.
        """
        if self.nip == other.nip and self.nom == other.nom and self.prenom == other.prenom :
            
            return True
        else:
            return False
        

    def __lt__(self, other) -> bool:
        """
        Renvoie True si self est né avant other
        """
        
        return Date.__lt__(self.naissance,other.naissance)
        

    def __str__(self) -> str:
        """
        Renvoie une représentation textuelle de self.
        """
        
        return self.prenom + " " + self.nom

    def __repr__(self) -> str:
        """
        Renvoie une représentation textuelle interne de self pour le shell.
        """
        return str(self.nip) + " : " + self.prenom + " " + self.nom.upper()
        
if (__name__ == "__main__"):
    import apl1test
    apl1test.testmod('etudiant.py')
    
    
def charge_fichier_etudiants(fname: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants présents dans le fichier dont
    le nom est donné en paramètre.

    précondition: le fichier est du bon format.
    """ 
    res = []
    with open(fname, 'r') as fin:
        fin.readline()
        ligne = fin.readline()
        while ligne != '':
            nip, nom, prenom, naissance, formation, groupe = ligne.strip().split(';')
            y, m, d = naissance.split('-')
            date_naiss = Date(int(d.lstrip('0')), int(m.lstrip('0')), int(y))
            res.append(Etudiant(nip, nom, prenom, date_naiss, formation, groupe))
            ligne = fin.readline()
    return res

    
etu = Etudiant(314159, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')
etu4 = Etudiant(141442, 'Calbuth', 'Raymond', Date(2,1,2005), 'MI', '11')



L_ETUDIANTS = charge_fichier_etudiants("etudiants.csv")
print(L_ETUDIANTS)
COURTE_LISTE = [L_ETUDIANTS[i] for i in range(10)]
print(COURTE_LISTE)
print(type(Etudiant(314159, 'Oléon', 'Tim', Date(7,11,2004), 'MI', '15')))
print(repr(etu))
print(etu.nom)


def est_liste_d_etudiants(x) -> bool:
    """
    Renvoie True si ``x`` est une liste de fiches d'étudiant, False dans le cas contraire.

    Précondition: aucune

    Exemples:

    $$$ est_liste_d_etudiants(COURTE_LISTE)
    True
    $$$ est_liste_d_etudiants("Timoleon")
    False
    $$$ est_liste_d_etudiants([('12345678', 'Calbuth', 'Raymond', 'Danse', '12') ])
    False
    """
    
    i = 0
    liste = []
    etu = Etudiant(0, 'A', 'A', Date(0,00,0000), 'A', '0')
    
    if type(x) == type(liste):
        for elt in x:
            if type(elt) != type(etu):
                i +=1     
    else:
        i +=1
        
    return i == 0
        
    


NBRE_ETUDIANTS = len(L_ETUDIANTS)
NIP = 42312241


print(L_ETUDIANTS[NIP%603])

    
MOINS_20_ANS = [L_ETUDIANTS[i] for i in range(NBRE_ETUDIANTS) if Date(2,2,2004) < L_ETUDIANTS[i].naissance]
    
    
print(MOINS_20_ANS)


def ensemble_des_formations(liste: list[Etudiant]) -> list[str]:
    """
    Renvoie un ensemble de chaînes de caractères donnant les formations
    présentes dans les fiches d'étudiants

    Précondition: liste ne contient que des fiches d'étudiants

    Exemples:

    $$$ ensemble_des_formations(COURTE_LISTE)
    ['MIASHS', 'MI', 'PEIP', 'LICAM']
    $$$ ensemble_des_formations(COURTE_LISTE[0:2])
    ['MIASHS', 'MI']
    """
    liste_formations = []
    for elt in liste:
        if elt.formation not in liste_formations:
            liste_formations.append(elt.formation)
    
    
    return liste_formations


print(ensemble_des_formations(L_ETUDIANTS))
print(ensemble_des_formations(COURTE_LISTE[0:2]))


def nb_occurrences_prenom(liste):
    """affiche le nombre d'occurrences d'un prénom dans une liste d'étudiants

    Précondition : Aucune
    Exemple(s) :
    $$$ nb_occurrences_prenom(COURTE_LISTE)
    {'Eugène': 1, 'Josette': 1, 'Benoît': 1, 'David': 2, 'Andrée': 1, 'Cécile': 1, 'Christiane': 1, 'Paul': 1, 'Anne': 1}

    """
    
    res = {}
    for etud in liste:
        prenom = etud.prenom
        if prenom in res:
            res[prenom] = res[prenom] + 1
        else:
            res[prenom] = 1
    return res


