#SPLIT
#1
s = "la méthode split est parfois bien utile"
s.split(' ')
#['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
s.split('e')
#['la méthod', ' split ', 'st parfois bi', 'n util', '']
s.split('é')
#['la m', 'thode split est parfois bien utile']
s.split()
#['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
#s.split('')
#Erreur séparateur vide
s.split('split')
#['la méthode ', ' est parfois bien utile']


#2. La méthode split sépare le texte entre chaque caractères ou
#chaine de caractères passée en argument et les ajoutent dans une liste


#3. Non elle ne modifie pas la chaîne à laquelle elle s'applique
#s.split(' ')
#print(s) renvoie "la méthode split est parfois bien utile"


#JOIN

#1

l = s.split()
print("".join(l))
#laméthodesplitestparfoisbienutile

print(" ".join(l))
#la méthode split est parfois bien utile

print(";".join(l))
#la;méthode;split;est;parfois;bien;utile

print(" tralala ".join(l))
#la tralala méthode tralala split tralala est tralala parfois tralala bien tralala utile

print("\n".join(l))
"""la
méthode
split
est
parfois
bien
utile
"""

print("".join(s))
#la méthode split est parfois bien utile

print("!".join(s))
#l!a! !m!é!t!h!o!d!e! !s!p!l!i!t! !e!s!t! !p!a!r!f!o!i!s! !b!i!e!n! !u!t!i!l!e

#print("".join())
#Erreur la méthode join doit avoir un argument

print("".join([]))
#


#print("".join([1,2]))
#Erreur les arguments doivent être des str et non des int


#2. La méthode join place entre chaque éléments de la liste un caractère ou une chaine de caractères donnés

#3. Non elle ne modifie pas la chaîne à laquelle elle s'applique
#print(l) renvoie ['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile'] soit l


#4.

def join(intercalaire : str,mots : list[str])->str:
    """renvoie une chaîne de caractères construite en concaténant toutes les chaînes de l en intercalant s

    Précondition : Aucune
    Exemple(s) :
    $$$ join('.', ['raymond', 'calbuth', 'ronchin', 'fr'])
    'raymond.calbuth.ronchin.fr'
    $$$ join(';',['a','b','c'])
    'a;b;c'

    """
    nouv_chaine = ""
    for i in range(len(mots)):
        nouv_chaine += mots[i] + intercalaire
    
    return nouv_chaine[:-1]


#SORT

#1.
l = list('timoleon')
l.sort()
print(l)
#['e', 'i', 'l', 'm', 'n', 'o', 'o', 't']

s = "Je n'ai jamais joué de flûte."
l = list(s)
l.sort()
print(l)
#[' ', ' ', ' ', ' ', ' ', "'", '.', 'J', 'a', 'a', 'a', 'd', 'e', 'e', 'e', 'f', 'i', 'i', 'j', 'j', 'l', 'm', 'n', 'o', 's', 't', 'u', 'é', 'û']

#sort range dans l'ordre alphabétique

#2.
# l = ['a', 1] affiche une erreur car sort ne peut pas comparer les int avec les str


def sort(s: str)->str:
    """Renvoie une chaîne de caractères contenant les caractères de `s` triés dans l'ordre croissant.


    Précondition : Aucune
    Exemple(s) :
    $$$ sort('timoleon')
    'eilmnoot'

    """
    nouv_chaine = ""
    liste = list(s)
    liste.sort()
    
    for i in range(len(liste)):
        nouv_chaine += liste[i]
        
        
    return nouv_chaine
    



def sont_anagrammes1(s1: str, s2: str) -> bool:
    """Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition : Aucune
    Exemple(s) :
    $$$ sont_anagrammes1('orange', 'organe')
    True
    $$$ sont_anagrammes1('orange','Organe')
    False
    
    """
    liste_mot1 = list(s1)
    liste_mot2 = list(s2)
    liste_mot1.sort()
    liste_mot2.sort()
    nb_identique = 0
    
    if len(liste_mot1) == len(liste_mot2):
        for i in range(len(liste_mot1)):
            if liste_mot1[i] == liste_mot2[i]:
                nb_identique += 1
        
        return nb_identique == len(liste_mot1)
            
    else:
        return False
    
    
def sont_anagrammes2(s1: str, s2: str) -> bool:
    """ Renvoie True si s1 et s2 sont anagrammatiques, False sinon.


    Précondition : Aucune
    Exemple(s) :
    $$$ sont_anagrammes2('orange', 'organe')
    True
    $$$ sont_anagrammes2('orange','Organe')
    False

    """
    occurrences_s1 = {}
    occurrences_s2 = {}
    nb_occurr_caractere1 = 0
    nb_occurr_caractere2 = 0
    
    if len(s1) == len(s1):
        for i in range(len(s1)):
            for j in range(len(s1)):
                caractere_actuel1 = s1[i]
                caractere_actuel2 = s2[i]
                if caractere_actuel1 == s1[j]:
                    nb_occurr_caractere1 += 1
                if caractere_actuel2 == s2[j]:
                    nb_occurr_caractere2 += 1
                    
            occurrences_s1[s1[i]] = nb_occurr_caractere1
            occurrences_s2[s2[i]] = nb_occurr_caractere2
            nb_occurr_caractere1 = 0
            nb_occurr_caractere2 = 0
        
        return occurrences_s1 == occurrences_s2
            
         




def sont_anagrammes3(s1: str, s2: str) -> bool:
    """Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition : Aucune
    Exemple(s) : 
    $$$ sont_anagrammes3('orange', 'organe')
    True
    $$$ sont_anagrammes3('orange','Organe')
    False

    """
    majuscules = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    minuscules = "abcdefghijklmnopqrstuvwxyz"
    nb_identique = 0
    equivalent_maj_min = {majuscules[i] : minuscules[i] for i in range(len(majuscules))}
    
    for i in range(len(s1)):
        if s1[i] in majuscules:
            carrac_actuel = equivalent_maj_min[s1[i]]
        else:
            carrac_actuel = s1[i]
            
        nb_occurr = s1.count(carrac_actuel)
        if nb_occurr == s2.count(carrac_actuel):
            nb_identique +=1
        
    return nb_identique == len(s1)
    


EQUIV_NON_ACCENTUE = {"a":"àâä","c" : "ç","e" : "éèêë","i":"îï","o":"ôö", "u" :"ùûü","y": "ÿ"}
    
    
def bas_casse_sans_accent(chaine : str)->str:
    """renvoie une chaîne de caractères identiques à celle passée en paramètre sauf pour les lettres majuscules et les lettres accentuées qui sont converties en leur équivalent minuscule non accentué. 

    Précondition : Aucune
    Exemple(s) :
    $$$ bas_casse_sans_accent('Orangé')
    'orange'
    $$$ bas_casse_sans_accent('Aéûo')
    'aeuo'

    """
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    chaine_minuscule = chaine.lower()
    nouv_chaine = ""
    
    
    
    for i in range(len(chaine)):
        if chaine_minuscule[i] in alphabet:
            nouv_chaine += chaine_minuscule[i]
        else:
            
            for values in EQUIV_NON_ACCENTUE:
                if chaine_minuscule[i] in EQUIV_NON_ACCENTUE[values]:
                    nouv_chaine += values
                
        
                
    return nouv_chaine
            

    

def sont_anagrammes4(s1: str, s2: str) -> bool:
    """Renvoie True si s1 et s2 sont anagrammatiques, False sinon.

    Précondition : Aucune
    Exemple(s) :
    $$$ sont_anagrammes1('orange', 'organe')
    True 
    $$$ sont_anagrammes1('orange','Organe')
    True
    
    """
    s11 = bas_casse_sans_accent(s1)
    s22 = bas_casse_sans_accent(s2)
    liste_mot1 = list(s11)
    liste_mot2 = list(s22)
    liste_mot1.sort()
    liste_mot2.sort()
    nb_identique = 0
    
    if len(liste_mot1) == len(liste_mot2):
        for i in range(len(liste_mot1)):
            if liste_mot1[i] == liste_mot2[i]:
                nb_identique += 1
        
        return nb_identique == len(liste_mot1)
            
    else:
        return False
    
    
sont_anagrammes4('orange','Organe')